import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {createPost} from '../action/postActions'

class PostForm extends Component {
    state = { title:'',body:'' ,userId:1}
    onChange=(e)=>{
    this.setState({[e.target.name]:e.target.value})
    }
    onSubmit =(e)=>{
        e.preventDefault(e);
        const post={
            title:this.state.title,
            body:this.state.body,
            userId:this.state.userId
        }
       //call action
       this.props.createPost(post);
    }
    render() { 
        return ( <div><h1>Add posts</h1>
        <form onSubmit={this.onSubmit}>
            <div>
                <label>Title:</label><br/>
                <input type="text" name="title" onChange={this.onChange} value={this.state.title}/>
                <br/>
            </div>
            <div>
                <label>Body:</label><br/>
                <textarea name="body" onChange={this.onChange} value={this.state.body}/>
                <br/>
            </div>
            <br/>
            <button type="submit">Submit</button>
        </form>
        </div> );
    }
}
PostForm.propTypes={
    createPost:PropTypes.func.isRequired
}
 
export default connect(null,{createPost})(PostForm);